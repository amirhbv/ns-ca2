import sys

import scapy.all as scapy


class Flow:
    def __init__(self, src_ip, dst_ip, src_port, dst_port, protocol, initial_packet) -> None:
        self.src_ip = src_ip
        self.dst_ip = dst_ip
        self.src_port = src_port
        self.dst_port = dst_port
        self.protocol = protocol
        self.packets = [initial_packet]

    def add_packet(self, packet):
        self.packets.append(packet)

    def __str__(self) -> str:
        return f'{self.src_ip},{self.dst_ip},{self.src_port},{self.dst_port},{self.protocol}'


class NetworkFlowExtractor:
    def __init__(self, pcap_file):
        self.packets = scapy.rdpcap(filename=pcap_file)
        self.flows = dict()

    def extract(self):
        for packet in self.packets:
            if packet.haslayer(scapy.IP):
                src_ip = packet[scapy.IP].src
                dst_ip = packet[scapy.IP].dst

                protocol = ''
                src_port = ''
                dst_port = ''
                if packet.haslayer(scapy.UDP):
                    protocol = 'UDP'
                    src_port = packet[scapy.UDP].sport
                    dst_port = packet[scapy.UDP].dport
                elif packet.haslayer(scapy.TCP):
                    protocol = 'TCP'
                    src_port = packet[scapy.TCP].sport
                    dst_port = packet[scapy.TCP].dport

                if protocol:
                    data1 = (src_ip, dst_ip, src_port, dst_port, protocol,)
                    data2 = (dst_ip, src_ip, dst_port, src_port, protocol,)
                    if data1 not in self.flows and data2 not in self.flows:
                        self.flows[data1] = Flow(*data1, packet)
                    elif data1 in self.flows:
                        self.flows[data1].add_packet(packet)
                    else:
                        self.flows[data2].add_packet(packet)

    def print_flows(self):
        print('srcip,dstip,srcport,dstport,protocol')
        for flow in self.flows.values():
            print(flow)




networkFlowExtractor = NetworkFlowExtractor(sys.argv[1])
networkFlowExtractor.extract()
networkFlowExtractor.print_flows()
