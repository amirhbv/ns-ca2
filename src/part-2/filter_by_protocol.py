import os
import sys

import scapy.all as scapy
from netfilterqueue import NetfilterQueue

scapy.load_layer('http')

with open(sys.argv[1], mode='r') as fin:
    protocol_blacklist = fin.read().splitlines()


def drop_by_protocol(pkt):
    packet = scapy.IP(pkt.get_payload())

    if 'DNS' in protocol_blacklist:
        if packet.haslayer(scapy.UDP) and packet[scapy.UDP].sport == 53 and packet.an and packet.an.type == 1:
            pkt.drop()
            return

    if 'HTTP' in protocol_blacklist:
        if packet.haslayer('HTTPRequest'):
            pkt.drop()
            return

    print(packet.summary())
    pkt.accept()

nfqueue = NetfilterQueue()
nfqueue.bind(1, drop_by_protocol)
os.system("iptables -I OUTPUT -j NFQUEUE --queue-num 1")
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')


os.system("iptables -D OUTPUT -j NFQUEUE --queue-num 1")
nfqueue.unbind()
