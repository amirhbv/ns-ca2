import os
import sys

import scapy.all as scapy
from netfilterqueue import NetfilterQueue


with open(sys.argv[1], mode='r') as fin:
    ips_blacklist = fin.read().splitlines()

with open(sys.argv[2], mode='r') as fin:
    ports_blacklist = [int(x) for x in fin.read().splitlines()]

def drop_by_ip_and_port(pkt):
    packet = scapy.IP(pkt.get_payload())

    if (packet[scapy.IP].dst in ips_blacklist) or (
        packet.haslayer(scapy.UDP) and packet[scapy.UDP].dport in ports_blacklist ) or (
            packet.haslayer(scapy.TCP) and packet[scapy.TCP].dport in ports_blacklist):
                print(packet.summary())
                pkt.drop()
                return

    pkt.accept()

nfqueue = NetfilterQueue()
nfqueue.bind(1, drop_by_ip_and_port)
os.system("iptables -I OUTPUT -j NFQUEUE --queue-num 1")
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')

os.system("iptables -D OUTPUT -j NFQUEUE --queue-num 1")
nfqueue.unbind()
