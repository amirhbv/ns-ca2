import os

import scapy.all as scapy
from netfilterqueue import NetfilterQueue

def print_and_accept(pkt):
    packet = scapy.IP(pkt.get_payload())
    print(packet.summary())

    pkt.accept()

nfqueue = NetfilterQueue()
nfqueue.bind(1, print_and_accept)
os.system("iptables -I OUTPUT -j NFQUEUE --queue-num 1")
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')

os.system("iptables -D OUTPUT -j NFQUEUE --queue-num 1")
nfqueue.unbind()
