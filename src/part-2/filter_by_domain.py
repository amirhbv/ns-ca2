import os
import sys

import scapy.all as scapy
from netfilterqueue import NetfilterQueue
from scapy.layers.tls.extensions import TLS_Ext_ServerName
from scapy.layers.tls.handshake import TLSClientHello

scapy.load_layer('tls')

with open(sys.argv[1], mode='r') as fin:
    domains_blacklist = fin.read().splitlines()

print(domains_blacklist)

def drop_by_domain(pkt):
    packet = scapy.IP(pkt.get_payload())

    domain = ''
    if packet.haslayer(scapy.UDP) and packet[scapy.UDP].sport == 53 and packet.an and packet.an.type == 1:
        domain = packet[scapy.DNSQR].qname.decode()[:-1]

    if packet.haslayer('TLS'):
        for msg in packet['TLS'].msg:
            if isinstance(msg, TLSClientHello):
                # Always 4th packet of flow
                for ext in msg.ext:
                    if isinstance(ext, TLS_Ext_ServerName) and ext.servernameslen > 0:
                        domain = ext.servernames[0].servername.decode()

    if domain:
        print(domain)
        if domain in domains_blacklist:
            print(packet.summary())
            pkt.drop()
            return

    pkt.accept()

nfqueue = NetfilterQueue()
nfqueue.bind(1, drop_by_domain)
os.system("iptables -I OUTPUT -j NFQUEUE --queue-num 1")
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')


os.system("iptables -D OUTPUT -j NFQUEUE --queue-num 1")
nfqueue.unbind()
